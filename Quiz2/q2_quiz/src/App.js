import { Box, AppBar, Toolbar, Typography, Button, Card, CardContent } from '@mui/material';
import { useState } from "react";
import { Routes, Route } from "react-router-dom";
import CheckNumber from './page/CheckNumber';
import Header from './components/Header';
import AboutMePage from './page/AboutMePage';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        
        <Route path="/" element={
          <CheckNumber />
        } />

        <Route path="about" element={
          <AboutMePage />

        } />
      </Routes>

    </div>
  );
}

export default App;
