import { Box, Paper } from '@mui/material';

function AboutMe(props) {
    return(
        <Box sx ={{width: '50%',height: '1000%'}}>
            <Paper elevation={10}>
                        <h3> จัดทำโดย: {props.name} </h3>
                        <h3> รหัสนักศึกษา: {props.id} </h3>
                        <h3> คณะ: {props.faculty} </h3>
                        <h3> ภาควิชา: {props.subfaculty} </h3>
            </Paper>
        </Box>
    );
}

export default AboutMe;