import { Box, AppBar, Toolbar, Typography, Button, Card, CardContent } from '@mui/material';
import { Link } from "react-router-dom";

function Header() {
    return (

        <Box>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6">
                        Find Even and Odd Number :
                    </Typography>
                    &nbsp;&nbsp;&nbsp;
                    <Link to="/">
                        <Typography variant="boby1" color="#6169e8">
                            ค้นหาจำนวนคู่คี่
                        </Typography>
                    </Link>
                    &nbsp;&nbsp;&nbsp;

                    <Link to="/about">
                        <Typography variant="boby1" color="#6169e8">
                            ผู้จัดทำ
                        </Typography>
                    </Link>

                </Toolbar>

            </AppBar>
        </Box>
    );
}

export default Header;