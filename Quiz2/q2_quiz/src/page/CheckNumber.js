import { Box, AppBar, Toolbar, Typography, Button, Card, CardContent } from '@mui/material';
import { useState } from "react";

function CheckNumber() {

    const [result, setResult] = useState(0);
    const [number, setNumber] = useState("");

    function checkNumber() {
        let n = parseInt(number);
        setResult(n);
        if (n % 2 == 0) {
            setResult("เลขคู่")
        }
        if (n % 2 == 1) {
            setResult("เลขคี่")
        }
        if (n == 0) {
            setResult("เลข 0")
        }
    }

    return (

        <Box>
            <Box sx={{ justifyContent: "center", marginTop: "20px", marginLeft: "40px", width: "50%" }}>
                Number: <input type="text"
                    value={number}
                    onChange={
                        (e) => {
                            setNumber(e.target.value);
                        }
                    } /> &nbsp;&nbsp;&nbsp;


                <Button variant="contained" onClick={() => { checkNumber() }} >ตรวจสอบ</Button>
            </Box>


            {result != 0 &&
                <Box sx={{ justifyContent: "left", marginTop: "20px", marginLeft: "40px", width: "50%", display: "flex", color: "#0b0d2d" }}>
                    <Card sx={{ minWidth: 342 }}>
                        <CardContent>
                            <Typography variant="body2" component="div">
                                ผลการตรวจสอบ
                            </Typography>
                            <Typography variant="body2" component="div">
                                {number} เป็น {result}
                            </Typography>
                        </CardContent>
                    </Card>
                </Box>
            }
        </Box>
    );
}

export default CheckNumber;