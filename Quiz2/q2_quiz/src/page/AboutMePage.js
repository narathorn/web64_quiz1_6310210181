import AboutMe from "../components/AboutMe";

function AboutMePage (){

    return(
     <ddiv>
         <div align="center">
             <h2> ผู้จัดทำ เว็บนี้ </h2>
        
             <AboutMe name="นราธร หนูพุ่ม"
               id="6310210181"
               faculty="วิทยาศาสตร์"
               subfaculty="วิทยาการคอมพิวเตอร์" />
            
         </div>
     </ddiv>   
    );
}

export default AboutMePage;